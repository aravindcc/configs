#!/bin/dash

curDisplayId=$(yabai -m query --displays --display | jq -re ".index")
open -a Terminal ~/Desktop
sleep 0.05
windowID=$(yabai -m query --windows --window | jq -re ".id")
$(yabai -m window --display $curDisplayId $windowID)
$(yabai -m window focus $windowID)