#!/bin/bash

cmdid='yabai -m query --spaces | jq -re ".[] | select(.visible == 1).index" | xargs -I{} yabai -m query --windows --space {} | jq -sre "add | sort_by(.display, .frame.x, .frame.y, .id) | map(select(.visible == 1)) | nth(index(map(select(.focused == 1 ))) - 1).id"'
id=$(eval "$cmdid")
cmd='echo $id | xargs -I{} yabai -m query --windows --window {} | jq ".frame"'
frame=$(eval "$cmd")

x=`echo "$frame" | jq ".x"`
y=`echo "$frame" | jq ".y"`
w=`echo "$frame" | jq ".w"`
h=`echo "$frame" | jq ".h"`
cx=$((x+w/2))
cy=$((y+h/2))
~/.config/scripts/click -x "$cx" -y "$cy"
yabai -m window --focus "$id"