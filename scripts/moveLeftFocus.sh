#!/bin/dash
# curWindowId="$(jq -re ".id" <<<$(yabai -m query --windows --window))"
xx=$(yabai -m query --windows --window)
curWindowId="$(echo $xx | jq -re ".id")"

focusWindow() {
    $(yabai -m window --focus $1) # $1 is the first argument passed in (window id).
}

$(yabai -m window --display prev || yabai -m window --display last)

cmd='echo $curWindowId | xargs -I{} yabai -m query --windows --window {} | jq ".frame"'
frame=$(eval "$cmd")

x=`echo "$frame" | jq ".x"`
y=`echo "$frame" | jq ".y"`
w=`echo "$frame" | jq ".w"`
h=`echo "$frame" | jq ".h"`
cx=$((x+w/2))
cy=$((y+h/2))
~/.config/scripts/click -x "$cx" -y "$cy"
focusWindow "$curWindowId"