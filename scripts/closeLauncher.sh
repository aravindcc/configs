#!/usr/bin/env bash

id=$(ps aux | grep -v grep | grep -i alacritty | awk '{print $2;}')
window_title=$(yq e ".window.title" ~/.config/alacritty/alacritty.yml)

if [[ "$id" == "$YABAI_RECENT_PROCESS_ID" && "$window_title" == "mylauncher" ]] 
then
    ~/.config/scripts/keyboard
    ~/.config/scripts/keyboard
    ~/.config/scripts/desume
fi