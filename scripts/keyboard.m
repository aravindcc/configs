#import <Foundation/Foundation.h>
#import <ApplicationServices/ApplicationServices.h>

// gcc -o keyboard keyboard.m -framework ApplicationServices -framework Foundation

int main(int argc, char *argv[]) {
  NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
  
  CFArrayRef windowList             = CGWindowListCopyWindowInfo(kCGWindowListOptionOnScreenOnly, kCGNullWindowID);
  ProcessSerialNumber psn         = {kNoProcess, kNoProcess};

  for (NSMutableDictionary* entry in (NSArray*) windowList)
  {
      int pid = [[entry objectForKey:(id)kCGWindowOwnerPID] intValue];
      NSString* applicationName = [entry objectForKey:(id)kCGWindowOwnerName];
      if ([applicationName isEqualToString:@"alacritty"]) {
        GetProcessForPID(pid, &psn);

        CGEventRef a = CGEventCreateKeyboardEvent(NULL, (CGKeyCode)8, true);  
        CGEventRef b = CGEventCreateKeyboardEvent(NULL, (CGKeyCode)8, false);  

        CGEventSetFlags(a, kCGEventFlagMaskControl);
        CGEventSetFlags(b, kCGEventFlagMaskControl);
        
        CGEventPostToPSN (&psn,a);
        CGEventPostToPSN (&psn,b);
        CFRelease(a);
        CFRelease(b);
        break;
      }
  }
  [pool release];
  return 0;
}