// Found on: http://hints.macworld.com/article.php?story=2008051406323031

// File: 
// click.m
//
// Compile with: 
// gcc -o click click.m -framework ApplicationServices -framework Foundation
//
// Usage:
// ./click -x pixels -y pixels 
// At the given coordinates it will click and release.


#import <Foundation/Foundation.h>
#import <ApplicationServices/ApplicationServices.h>


int main(int argc, char *argv[]) {
  NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];

  // The data structure CGPoint represents a point in a two-dimensional
  // coordinate system.  Here, X and Y distance from upper left, in pixels.
  //
  CGPoint pt;
  pt.x = atoi(argv[2]);
  pt.y = atoi(argv[4]);
  // printf("%f, %f", pt.x, pt.y);


  // This is where the magic happens.  See CGRemoteOperation.h for details.
  //
  // CGPostMouseEvent( CGPoint        mouseCursorPosition,
  //                   boolean_t      updateMouseCursorPosition,
  //                   CGButtonCount  buttonCount,
  //                   boolean_t      mouseButtonDown, ... )
  //
  // So, we feed coordinates to CGPostMouseEvent, put the mouse there,
  // then click and release.
  //
  CGEventType et = kCGEventMouseMoved;
  CGMouseButton mb = kCGMouseButtonLeft;
  int mouseEventTapLocation = kCGSessionEventTap;

  CGEventRef ev = CGEventCreateMouseEvent(NULL, et, pt, mb);
  CGEventSetLocation(ev, pt);
  CGEventSetFlags(ev, 0);
  CGEventSetType(ev, et);
  CGEventPost(mouseEventTapLocation, ev);

  [pool release];
  return 0;
}