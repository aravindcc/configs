#import <Foundation/Foundation.h>
#import <ApplicationServices/ApplicationServices.h>
#import <Cocoa/Cocoa.h>
// gcc -o desume desume.m -framework ApplicationServices -framework Foundation -framework Cocoa

void hideAlacritty() {
  CFArrayRef windowList = CGWindowListCopyWindowInfo(kCGWindowListOptionAll, kCGNullWindowID);
  for (NSMutableDictionary* entry in (NSArray*) windowList)
  {
    int pid = [[entry objectForKey:(id)kCGWindowOwnerPID] intValue];
    NSString* applicationName = [entry objectForKey:(id)kCGWindowOwnerName];
    if ([applicationName isEqualToString:@"alacritty"]) {
      NSRunningApplication* app = [NSRunningApplication runningApplicationWithProcessIdentifier: pid];
      [app hide];
      return;
    }
  }
}

int main(int argc, char *argv[]) {
  NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
  
  hideAlacritty();

  [pool release];
  return 0;
}