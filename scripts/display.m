/*
 * To compile objective-c on the command line:
 *
 * clang -framework Foundation display.m -o display
 *
 * You may have to link with -lobjc or other libs,
 * as required.
 */

#import <Foundation/Foundation.h>
#import <AppKit/AppKit.h>

int main(int argc, char** argv)
{
  NSArray*screens=[NSScreen screens];
  int i=1;
  for(NSScreen*screen in screens){
      printf("%i: %f\n", i, screen.backingScaleFactor);
      i++;
  }
}