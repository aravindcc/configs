#!/bin/bash

id=$(ps aux | grep -v grep | grep -i alacritty | awk '{print $2;}')
window_title=$(yq e ".window.title" ~/.config/alacritty/alacritty.yml)

if [[ -n "$id" && "$window_title" != "mylauncher" ]]; then
    paste=$(pbpaste)
    reference=$(osascript ~/.config/scripts/browserURL.scpt)
    if [[ $reference == " ()" ]]; then
        app=$(osascript -e 'tell application "System Events" to get name of application processes whose frontmost is true and visible is true')
        if [[ "Electron" == $app ]]; then
            reference=$(cat ~/.config/watcher/tmp.txt)
        fi
    fi
    nl=$'\r'
    echo "$nl$paste$nl[$reference]$nl" > ~/.config/watcher/vim.txt
fi

