#import <Foundation/Foundation.h>
#import <ApplicationServices/ApplicationServices.h>
#import <Cocoa/Cocoa.h>
// gcc -o consume consume.m -framework ApplicationServices -framework Foundation


// A linked list (LL) node to store a queue entry
struct QNode {
    CGEventRef key;
    struct QNode* next;
};
 
// The queue, front stores the front node of LL and rear stores the
// last node of LL
struct Queue {
    struct QNode *front, *rear;
};
 
// A utility function to create a new linked list node.
struct QNode* newNode(CGEventRef k)
{
    struct QNode* temp = (struct QNode*)malloc(sizeof(struct QNode));
    temp->key = k;
    temp->next = NULL;
    return temp;
}
 
// A utility function to create an empty queue
struct Queue* createQueue()
{
    struct Queue* q = (struct Queue*)malloc(sizeof(struct Queue));
    q->front = q->rear = NULL;
    return q;
}
 
// The function to add a key k to q
void enQueue(struct Queue* q, CGEventRef k)
{
    // Create a new LL node
    struct QNode* temp = newNode(k);
 
    // If queue is empty, then new node is front and rear both
    if (q->rear == NULL) {
        q->front = q->rear = temp;
        return;
    }
 
    // Add the new node at the end of queue and change rear
    q->rear->next = temp;
    q->rear = temp;
}
 
// Function to remove a key from given queue q
CGEventRef deQueue(struct Queue* q)
{
    // If queue is empty, return NULL.
    if (q->front == NULL)
        return NULL;
 
    // Store previous front and move front one node ahead
    struct QNode* temp = q->front;
 
    q->front = q->front->next;
 
    // If front becomes NULL, then change rear also as NULL
    if (q->front == NULL)
        q->rear = NULL;
 
    CGEventRef ev = temp->key;
    free(temp);
    return ev;
}



int getAlacritty(ProcessSerialNumber* inPsn) {
  static ProcessSerialNumber psn         = {kNoProcess, kNoProcess};
  static int gotPSN = 0;
  if (gotPSN == 1) {
    *inPsn = psn; 
    return 1;
  }

  CFArrayRef windowList = CGWindowListCopyWindowInfo(kCGWindowListOptionAll, kCGNullWindowID);
  for (NSMutableDictionary* entry in (NSArray*) windowList)
  {
    int pid = [[entry objectForKey:(id)kCGWindowOwnerPID] intValue];
    NSString* applicationName = [entry objectForKey:(id)kCGWindowOwnerName];
    if ([applicationName isEqualToString:@"alacritty"]) {
      GetProcessForPID(pid, &psn);
      gotPSN = 1;
      *inPsn = psn; 
      return 1;
    }
  }
  return 0;
}

void unhideAlacritty() {
  CFArrayRef windowList = CGWindowListCopyWindowInfo(kCGWindowListOptionAll, kCGNullWindowID);
  for (NSMutableDictionary* entry in (NSArray*) windowList)
  {
    int pid = [[entry objectForKey:(id)kCGWindowOwnerPID] intValue];
    NSString* applicationName = [entry objectForKey:(id)kCGWindowOwnerName];
    if ([applicationName isEqualToString:@"alacritty"]) {
      NSRunningApplication* app = [NSRunningApplication runningApplicationWithProcessIdentifier: pid];
      [app unhide];
      return;
    }
  }
}

CGEventRef myCGEventCallback(CGEventTapProxy proxy, CGEventType type, CGEventRef event, void *refcon) {

  static struct Queue* delayQueue = NULL;
  static CGEventTimestamp processed = 0;

  if (CGEventGetTimestamp(event) < processed) return NULL;

  if (delayQueue == NULL) {
    delayQueue = createQueue();
  }

  ProcessSerialNumber psn = {0, 0};
  int status = getAlacritty(&psn);

  if (status) {
    // post all queued events
    CGEventRef prev = deQueue(delayQueue);
    while (prev != NULL) {
      processed = CGEventGetTimestamp(prev);
      CGEventPostToPSN(&psn, prev);
      prev = deQueue(delayQueue);
    }
    processed = CGEventGetTimestamp(event);
    CGEventPostToPSN(&psn, event);
  } else {
    // add to linked list.
    enQueue(delayQueue, event);
  }
  return NULL;
}

int main(int argc, char *argv[]) {
  NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
  
  unhideAlacritty();

  CFRunLoopSourceRef runLoopSource;

  CFMachPortRef eventTap = CGEventTapCreate(kCGHIDEventTap, kCGHeadInsertEventTap, kCGEventTapOptionDefault, CGEventMaskBit(kCGEventKeyDown), myCGEventCallback, NULL);

  if (!eventTap) {
    NSLog(@"Couldn't create event tap!");
    exit(1);
  }

  runLoopSource = CFMachPortCreateRunLoopSource(kCFAllocatorDefault, eventTap, 0);
  CFRunLoopAddSource(CFRunLoopGetCurrent(), runLoopSource, kCFRunLoopCommonModes);
  CGEventTapEnable(eventTap, true);

  NSTimeInterval delayInSeconds = 3;
  dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
  dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
    CFRunLoopStop(CFRunLoopGetCurrent());
  });

  CFRunLoopRun();
  CFRelease(eventTap);
  CFRelease(runLoopSource);

  [pool release];
  return 0;
}