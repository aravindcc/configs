import sys
import os 

def camel(s):
    return ''.join(x for x in s.title() if not x.isspace())

def main():
    dir_path = os.path.dirname(os.path.realpath(__file__))
    tag_file = open(f"{dir_path}/tags", "r")
    content = tag_file.read().splitlines()
    tag_file.close()
    args = list(set(sys.argv[1:]))
    args.sort(key=lambda a: content.index(a))
    rel_path = "/".join(args)
    path = f"{dir_path}/{rel_path}"
    os.makedirs(path, exist_ok=True)
    var = input("Name of the note: ")
    notePath = f"{path}/{camel(var)}"
    try:
       os.makedirs(notePath, exist_ok=True) 
    except:
        return

    extension_map = {
        "python": "py",
        "javascript": "ts",
        "terraform": "tf",
        "ocaml": "ml",
        "sql": "sql",
        "c++": "cpp",
        "scss": "scss",
        "dart": "dart",
        "swift": "swift",
        "django": "py",
        "react": "tsx",
        "react-native": "tsx",
        "nest": "ts",
        "docker": "~Dockerfile",
        "tailwind": "tsx",
        "algorithm": "py",
    }

    for arg in args:
        if arg in extension_map:
            open(f"{notePath}/code.{extension_map[arg]}", "w+").close()
    
    explain = open(f"{notePath}/explain.md", "w+")
    explain.write(f"# {var}\n")
    explain.close()

    cmd = f"{dir_path}/../bin/options/note.sh \"{notePath}/explain.md\""
    os.system(cmd)

if __name__ == "__main__":
    main()