#!/usr/bin/env bash
while true; do 
    path=$(find ~/.config/bin/options -regex '.*\.sh' | fzf --expect ' ' --delimiter / --with-nth -1)
    eval $path
    ~/.config/scripts/desume
done