#!/usr/bin/env bash

~/.config/scripts/consume&
id=$(ps aux | grep -v grep | grep -i alacritty | awk '{print $2;}')
if [[ -z "$id" ]]; then 
    TITLE=mylauncher

    width=$(yq e ".launcher.width" ~/.config/alacritty/alacritty.yml)
    height=$(yq e ".launcher.height" ~/.config/alacritty/alacritty.yml)

    yq e -i ".window.title = \"$TITLE\"" ~/.config/alacritty/alacritty.yml

    if [[ -z "$width" ]]; then
        alacritty -o window.dimensions.lines=20 -o window.dimensions.columns=60 -o window.decorations=none --working-directory "$(pwd)" -e "$1"
        yabai_window=$(yabai -m query --windows | jq -r ".[] | select(.pid == $id)")
        w=$(echo "$yabai_window" | jq -r ".frame.w")
        h=$(echo "$yabai_window" | jq -r ".frame.h")
        yq e -i ".launcher.width = \"$w\"" ~/.config/alacritty/alacritty.yml
        yq e -i ".launcher.height = \"$h\"" ~/.config/alacritty/alacritty.yml
    else
        display="$(yabai -m query --displays --display)"
        display_index=$(echo "$display" | jq -r ".index")
        s="$(yq e ".$display_index" ~/.config/alacritty/display.yml)"
        x=$(jq \
        --argjson display "${display}" \
        --argjson width "${width}" \
        --argjson s "${s}" \
        -nr '((($display.frame | .x + .w / 2) - ($width / 2)) * $s)')
        y=$(jq \
        --argjson display "${display}" \
        --argjson height "${height}" \
        --argjson s "${s}" \
        -nr '((($display.frame | .y + .h / 2) - ($height / 2)) * $s)')
        alacritty -o window.position.x="${x}" -o window.position.y="${y}" -o window.dimensions.lines=20 -o window.dimensions.columns=60 -o window.decorations=none --working-directory "$(pwd)" -e "$1"
    fi
else
    yabai_window=$(yabai -m query --windows | jq -r ".[] | select(.pid == $id)" | jq -r ".id")
    yabai -m window --focus $yabai_window
fi

