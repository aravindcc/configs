#!/usr/bin/env bash

find ~/Documents ~/Desktop ~/Downloads -type f -not -path '*/node_modules*' \
       -a -not -path '*.git*'               \
       -a -not -name '*~' | fzf | xargs -I {} open "{}"