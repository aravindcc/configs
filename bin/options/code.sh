#!/usr/bin/env bash

fs=$(find ~/Desktop -maxdepth 1 -mindepth 1 -not -path '*/\.*')
extra=('~/.config')
add=""
for i in "${extra[@]}"
do
   p=$(eval "find $i -maxdepth 0")
   add+=$'\n'
   add+=$p
done
echo "$fs$add" | fzf | xargs -I {} code "{}"