#!/usr/bin/env bash

# make sure you have ripgrep installed

folder_core="find ~/.config/notes -regex '.*/explain.md'"
folder="$folder_core"
cmd="fzf --expect=ctrl-f --delimiter / --with-nth 6..-2 --print-query"
out=""

if [ -z "$1" ]; then
    while true; do
        out=$(eval "${folder} | ${cmd}")
        if [[ $out == *"ctrl-f"* ]]; then
            q=$(echo "$out" | sed -n '1p')
            if [ -z "$q" ]
            then
                folder="$folder_core"
            else
                folder="rg -l -i '${q}'  ~/.config/notes"
            fi
        elif [[ $folder != $folder_core  ]] && [ -z "$out" ]
        then
            folder="$folder_core"
        else
            break
        fi
    done
else 
    out="$1"
fi

if [ -z "$out" ]; then
    tags=()
    while true; do
        tag=$(cat ~/.config/notes/tags | fzf --expect ' ')
        if [ -z "$tag" ]; then
            break
        else
            tags+=( $tag )
        fi
    done
    if (( ${#tags[@]} )); then
        python3 ~/.config/notes/creator.py "${tags[@]}"
    fi
else
    id=$(ps aux | grep -v grep | grep -i alacritty | awk '{print $2;}')
    window=$(yabai -m query --windows | jq -r ".[] | select(.pid == $id).id")
    yabai -m window --resize bottom_right:200:100 $window
    yabai -m window --resize top_left:-200:-100 $window
    folder="$(dirname "$out" | xargs)"
    folder_name="$(basename "$folder" | xargs)"
    yq e -i ".window.title = \"$folder_name\"" ~/.config/alacritty/alacritty.yml
    
    code_fs="$(find $folder -type f -not -name 'explain.md' -not -name 'code.py')"
    SAVEIFS=$IFS   # Save current IFS
    IFS=$'\n'      # Change IFS to new line
    names=($code_fs) # split to array $names
    IFS=$SAVEIFS   # Restore IFS
    extra_tabs=""
    for (( i=0; i<${#names[@]}; i++ ))
    do
        ex="${names[$i]//$folder/}"
        extra_tabs+="-c 'e ${ex:1}' "
    done
    extra_tabs+="-c 'b code.py'"
    cd $folder
    cmd="vim -O code.py explain.md $extra_tabs -c 'so ~/.config/watcher/watch.vim'"
    eval $cmd
fi