func MyHandler()
    let text = system('~/.config/watcher/vim-watch.sh')
    if len(text) != 0
        exe "normal! a" . text . "\<Esc>"
    endif
    :redraw!
endfunc
nnoremap ` :call MyHandler()<CR>