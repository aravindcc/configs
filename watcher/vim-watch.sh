#!/usr/bin/env bash

prev=$(cat ~/.config/watcher/vim-help.txt)
tmp=$(date -r ~/.config/watcher/vim.txt)
if [[ $prev != $tmp ]]; then
    txt=$(cat ~/.config/watcher/vim.txt) 
    if [[ -n txt ]]; then
        > ~/.config/watcher/vim.txt 
        echo $txt
    fi
    date -r ~/.config/watcher/vim.txt > ~/.config/watcher/vim-help.txt
fi