"use strict";

import { window, workspace } from "vscode";

export class Config {
  private readonly _config: any;

  private readonly _defaultPathStyle: string = "";
  public get defaultPathStyle(): string {
    return this._defaultPathStyle;
  }

  private readonly _priorityInStatusBar: number = 0;
  public get priorityInStatusBar(): number {
    return this._priorityInStatusBar;
  }

  private readonly _defaultPathStartsFrom: string = "";
  public get defaultPathStartsFrom(): string {
    return this._defaultPathStartsFrom;
  }

  constructor() {
    try {
      this._defaultPathStyle = "unix";
      this._priorityInStatusBar = 0;
      this._defaultPathStartsFrom = "rootDirectory";
    } catch (ex) {
      window.showErrorMessage(ex.message);
    }
  }
}
