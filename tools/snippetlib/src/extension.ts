// The module 'vscode' contains the VS Code extensibility API
// Import the module and reference it with the alias vscode in your code below
import * as vscode from "vscode";
import { CurrentFile } from "./currentFile";
import { EditorChangeListener } from "./editorChangeListener";

// this method is called when your extension is activated
// your extension is activated the very first time the command is executed
export function activate(context: vscode.ExtensionContext) {
  // Use the console to output diagnostic information (console.log) and errors (console.error)
  // This line of code will only be executed once when your extension is activated
  console.log("Acitvated Snippet Extension");
  let currentFile = new CurrentFile();
  let listener = new EditorChangeListener(currentFile);
  context.subscriptions.push(listener);
  context.subscriptions.push(currentFile);
  console.log("Initialised Snippet Extension");
}

// this method is called when your extension is deactivated
export function deactivate() {}
